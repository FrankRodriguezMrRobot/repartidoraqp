import { Component, OnInit } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})
export class MapaPage implements OnInit {
  public latitude: number;
  public longitude: number;
  public origin: any;
  public destination: any;
  public dataOrder;
  constructor(private activatedRoute: ActivatedRoute) { 
    this.latitude = -16.399619;
    this.longitude = -71.521628;
    this.getParameters();
  }

  ngOnInit() {
    //this.getDirection();
  }
  getDirection(oLat,oLng,dLat,dLng) {
    // this.origin = { lat: -16.404272, lng: -71.543205 };
    // this.destination = { lat: -16.405089, lng: -71.514674 };
    this.origin = { lat: +oLat, lng: +oLng };
    this.destination = { lat: +dLng, lng: +dLat };
    console.warn("ORIGN: ",this.origin);
    console.warn("DESTI: ",this.destination);
  }
  getParameters(){
    this.activatedRoute.queryParams.subscribe((res)=>{
      this.dataOrder = res;
      console.error(res);
      this.separateCoords(this.dataOrder);
    });
  }
  separateCoords(dataOrder){
    //console.warn("FIRST:LAT: ", dataOrder.restaurant_long_lat.substring(0,dataOrder.restaurant_long_lat.indexOf('|')) , "LNG: ",dataOrder.restaurant_long_lat.substring(dataOrder.restaurant_long_lat.indexOf('|')+1));
    //console.warn("SECOND:LAT: ", dataOrder.user_long_lat.substring(0,dataOrder.user_long_lat.indexOf('|')) , "LNG: ",dataOrder.user_long_lat.substring(dataOrder.user_long_lat.indexOf('|')+1));
    this.getDirection(
      dataOrder.restaurant_long_lat.substring(0,dataOrder.restaurant_long_lat.indexOf('|')),
      dataOrder.restaurant_long_lat.substring(dataOrder.restaurant_long_lat.indexOf('|')+1),
      dataOrder.user_long_lat.substring(0,dataOrder.user_long_lat.indexOf('|')),
      dataOrder.user_long_lat.substring(dataOrder.user_long_lat.indexOf('|')+1)
    )
  }
  

}
