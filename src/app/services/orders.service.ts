import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import * as Global from '../globals';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private baseUrl = Global.api_url;

  constructor(public http: HttpClient) { }
  getOrder(orderNumber){
    //5683
    const requestOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    let request = this.http.get(this.baseUrl+ '/api/order_details/' + orderNumber, requestOptions);
    return request;
  }
}
