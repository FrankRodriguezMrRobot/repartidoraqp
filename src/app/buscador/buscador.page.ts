import { Component, OnInit } from '@angular/core';

import { OrdersService } from './../services/orders.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.page.html',
  styleUrls: ['./buscador.page.scss'],
})
export class BuscadorPage implements OnInit {
  public orderNumber;
  public orderData;
  constructor(private ordersService:OrdersService, private router: Router) { 

  }

  ngOnInit() {
    this.getOrder();
  }

  getOrder(){
    this.ordersService.getOrder(this.orderNumber).subscribe((res)=>{
      this.orderData = res[0];
      console.info("ORDERDATA: ",res);
    })
  }
  openMap(){
    this.router.navigate(['/mapa'],{
      queryParams: this.orderData,
    });
  }

}
